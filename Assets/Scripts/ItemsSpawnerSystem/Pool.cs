using System;
using System.Collections;
using System.Collections.Generic;
using ItemsSpawnerSystem;
using UnityEngine;

public class Pool : MonoBehaviour
{
    [SerializeField] private Obstacle prefab;
    [SerializeField] private Transform poolParent;
    
    private List<Obstacle> activeEats = new List<Obstacle>();
    private List<Obstacle> reserveEats = new List<Obstacle>();

    public void CreatePool(int count, Action<Obstacle, CollisionType, ObstacleType, FartingBoosterType> action)
    {
        for (int i = 0; i < count; i++)
        {
            var obj = Instantiate(prefab, poolParent);
            obj.OnCollision += (eat, collObject, obstacleType, boosterType) =>
            {
                action?.Invoke(eat, collObject, obstacleType, boosterType);
            };
            obj.gameObject.SetActive(false);
            reserveEats.Add(obj);
        }
    }

    public void SpawnElement(Transform point)
    {
        reserveEats[0].transform.position = point.position;
        ToActiveEat(reserveEats[0]);
    }

    public void ToReserveEat(Obstacle obstacle)
    {
        activeEats.Remove(obstacle);
        reserveEats.Add(obstacle);
        obstacle.gameObject.SetActive(false);
    }
    
    public void ToActiveEat(Obstacle obstacle)
    {
        reserveEats.Remove(obstacle);
        activeEats.Add(obstacle);
        obstacle.Setup();
        obstacle.gameObject.SetActive(true);
    }
}
