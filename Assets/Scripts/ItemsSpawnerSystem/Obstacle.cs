﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Obstacle : MonoBehaviour
{
    [SerializeField] private List<Sprite> eatSprites;
    [SerializeField] private List<Sprite> characterSprites;
    [SerializeField] private Sprite herringSprite;
    [SerializeField] private Sprite cucumberSprite;
    [SerializeField] private Sprite milkSprite;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [Header("Chance = 1/x")]
    [SerializeField] private int characterChance;
    [Header("Chance = 1/x")]
    [SerializeField] private int boosterChance;
    [SerializeField] private int weightPlus = 5;
    [SerializeField] private List<GameObject> boosterVFXs;

    public static bool CharacterNow;
    public static bool boosterNow;
    public static bool cucumberComplete;
    public static bool herringComplete;
    public static bool milkComplete;
    
    public static List<FartingBoosterType> nowBoostersComplete = new List<FartingBoosterType>();

    public int WeightPlus => weightPlus;
    
    public Action<Obstacle, CollisionType, ObstacleType, FartingBoosterType> OnCollision;
    private ObstacleType obstacleType;
    private FartingBoosterType boosterType = FartingBoosterType.none;

    public void Setup()
    {
        if (!CharacterNow && UnityEngine.Random.Range(0, characterChance) == 0)
        {
            /*CharacterNow = true;
            obstacleType = ObstacleType.character;
            int randIndex = UnityEngine.Random.Range(0, characterSprites.Count);
            spriteRenderer.sprite = characterSprites[randIndex];*/
        }
        else if (!boosterNow && UnityEngine.Random.Range(0, boosterChance) == 0)
        {
            boosterNow = true;
            obstacleType = ObstacleType.booster;
            SelectBooster();
        }
        else
        {
            obstacleType = ObstacleType.eat;
            int randIndex = UnityEngine.Random.Range(0, eatSprites.Count);
            spriteRenderer.sprite = eatSprites[randIndex];
        }
    }

    private void SelectBooster()
    {
        foreach (var vfx in boosterVFXs)
        {
            vfx.SetActive(true);
        }
        List<FartingBoosterType> forSelect = new List<FartingBoosterType>();

        if (nowBoostersComplete.Count == 0)
        {
            spriteRenderer.sprite = cucumberSprite;
            boosterType = FartingBoosterType.cucumber;
        }

        foreach (var item in nowBoostersComplete)
        {
            if (!nowBoostersComplete.Contains(FartingBoosterType.cucumber))
            {
                forSelect.Add(FartingBoosterType.cucumber);
                break;
            }

            if (!nowBoostersComplete.Contains(FartingBoosterType.herring))
            {
                forSelect.Add(FartingBoosterType.herring);
                break;
            }

            if (!nowBoostersComplete.Contains(FartingBoosterType.milk))
            {
                forSelect.Add(FartingBoosterType.milk);
                break;
            }

        }

        if (forSelect.Count != 0)
        {
            int randIndex = UnityEngine.Random.Range(0, forSelect.Count);
            boosterType = forSelect[randIndex];
            SelectBooster(forSelect[randIndex]);
        }
        else
        {
            nowBoostersComplete.Clear();
        }
    }

    private void SelectBooster(FartingBoosterType type)
    {
        switch (type)
        {
            case FartingBoosterType.cucumber:
                boosterType = FartingBoosterType.cucumber;
                spriteRenderer.sprite = cucumberSprite;
                break;
            case FartingBoosterType.herring:
                boosterType = FartingBoosterType.herring;
                spriteRenderer.sprite = herringSprite;
                break;
            case FartingBoosterType.milk:
                boosterType = FartingBoosterType.milk;
                spriteRenderer.sprite = milkSprite;
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out EndPoint endPoint))
        {
            if (obstacleType == ObstacleType.booster)
                boosterNow = false;

            foreach (var vfx in boosterVFXs)
            {
                vfx.SetActive(false);
            }
            
            OnCollision?.Invoke(this, CollisionType.bottom, obstacleType, boosterType);
        }
        else if (other.TryGetComponent(out KingCharacterController testKing))
        {
            if (obstacleType == ObstacleType.booster)
            {
                GameManager.Instance.UIManager.SetPerfect();
                SetBoosterEffect(boosterType);
            }

            foreach (var vfx in boosterVFXs)
            {
                vfx.SetActive(false);
            }
            
            OnCollision?.Invoke(this, CollisionType.king, obstacleType, boosterType);
        }
    }

    private void SetBoosterEffect(FartingBoosterType type)
    {
        switch (type)
        {
            case FartingBoosterType.cucumber:
                GameManager.Instance.UIManager.SetCucumber();
                break;
            case FartingBoosterType.herring:
                GameManager.Instance.UIManager.SetHerring();
                break;
            case FartingBoosterType.milk:
                GameManager.Instance.UIManager.SetMilk();
                break;
        }
    }
}

public enum CollisionType
{
    king,
    bottom
}

public enum ObstacleType
{
    eat,
    character,
    booster,
}

public enum FartingBoosterType
{
    none,
    cucumber,
    herring,
    milk
}
