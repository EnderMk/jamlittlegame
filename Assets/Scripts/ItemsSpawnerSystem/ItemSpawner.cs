using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using ItemsSpawnerSystem;
using UnityEngine;
using Random = System.Random;

public class ItemSpawner : MonoBehaviour
{
    [SerializeField] private List<Transform> spawnPoints;
    [SerializeField] private Pool eatPool;
    [SerializeField] private int countPoolObjects;
    [SerializeField] private Vector2 cooldown;
    [SerializeField] private Vector2Int itemsOnPoint;
    [SerializeField] private float timeBetweenSpawnEat;
    
    public int Factor = 1;
    
    private List<Obstacle> eatsList;

    private Transform selectSpawnPoint;
    private int countOnPoint;
    private float nowTimeForSpawn;

    private bool _spawnElement = false;
    
    private void Awake()
    {
        eatPool.CreatePool(countPoolObjects, ToProcessCollision);
        //StartSpawn();
    }

    public void StartSpawn()
    {
        _spawnElement = true;
    }

    public void StopSpawn()
    {
        _spawnElement = false;
    }
    
    private void FixedUpdate()
    {
        if (!_spawnElement)
            return;
        TimeToSpawn();
        SetFactor();
    }

    private void ToProcessCollision(Obstacle obstacle,
        CollisionType collisionType,
        ObstacleType obstacleType,
        FartingBoosterType fartingBoosterType)
    {
        eatPool.ToReserveEat(obstacle);
        switch (collisionType)
        {
            case CollisionType.king:
                if (obstacleType == ObstacleType.character)
                {
                    //animation with king
                    Obstacle.CharacterNow = false;
                }
                else if (obstacleType == ObstacleType.booster)
                {
                    SetBoosterEffect(fartingBoosterType);
                }
                else if(obstacleType == ObstacleType.eat)
                {
                    GameManager.Instance.WeightManager.AddWeight(obstacle.WeightPlus);
                    AudioManager.Instance.PlaySFXSound(AudioManager.Instance.voiceKing);
                    //add to obesity scale
                }
                
                break;
            
            case CollisionType.bottom:
                if (obstacleType == ObstacleType.character)
                {
                    Obstacle.CharacterNow = false;
                }

                //nothing for now
                break;
        }
    }

    private void TimeToSpawn()
    {
        if (nowTimeForSpawn <= 0)
        {
            float randTimeToSpawn = UnityEngine.Random.Range(cooldown.x, cooldown.y);
            nowTimeForSpawn = randTimeToSpawn;
            SpawnEat();
        }
        else
        {
            nowTimeForSpawn -= Time.deltaTime;
        }
    }

    private void SetBoosterEffect(FartingBoosterType fartingBoosterType)
    {
        Obstacle.nowBoostersComplete.Add(fartingBoosterType);
        Obstacle.boosterNow = false;
        if (Obstacle.nowBoostersComplete.Count == 3)
        {
            EventManager.Instance.OnBoosterApply?.Invoke();
            //GameManager.Instance.WeightManager.ReternWeight();
            Obstacle.nowBoostersComplete.Clear();
        }
    }

    private void SpawnEat()
    {
        int randCount = UnityEngine.Random.Range(itemsOnPoint.x, itemsOnPoint.y);
        countOnPoint = randCount;
        int randPointIndex = UnityEngine.Random.Range(0, spawnPoints.Count);
        selectSpawnPoint = spawnPoints[randPointIndex];

        StartCoroutine(SpawnEatsPack(countOnPoint));
    }

    private void SetFactor()
    {
        nowTimeForSpawn *= Factor;
        timeBetweenSpawnEat *= Factor;
    }

    private IEnumerator SpawnEatsPack(int count)
    {
        for (int i = 0; i < count; i++)
        {
            eatPool.SpawnElement(selectSpawnPoint);
            yield return new WaitForSeconds(timeBetweenSpawnEat);
        }
    }
}
