using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.PlayerLoop;


public class KingCharacterController : MonoBehaviour, IHitObject
{
    [SerializeField] private Transform _characterBody;
    [SerializeField] private Transform _massCenter;
    //[SerializeField] private float _forwardOffset = 0.1f;
    [SerializeField] private float _froceUpPower = 100f;
    //[SerializeField] private float _froceSidePower = 100f;
    //[SerializeField] private float _limitVelocity = 10f;
    [SerializeField] private BoxCollider2D _characterCollider;
    [SerializeField] private GameObject _showRainbowEffect;
    [SerializeField] private GameObject _rainbowEffect;
    [SerializeField] private Animator _kingAnimator;

    //move step
    [SerializeField] private float _moveStep = 1f;
    [SerializeField] private float _moveTime = 1.5f;

    //limit slide
    [SerializeField] private float _limitSlide = 1f;

    private Rigidbody2D _kingRig;
    private Vector3 _beforSlidePos;

    private Tween _moveLeft;
    private Tween _moveRight;
    private Tween _angleTween;
    private Tween _angleBoostTween;

    
    private void Awake()
    {
        _kingRig = GetComponent<Rigidbody2D>();
    }
    
    
    # region tween move
    
    private void RotateToTarget(Vector3 target)
    {
        Ease easy = Ease.InOutQuad;
        float moveTime = _moveTime / 2;

        _angleTween = _characterBody.DORotate(target,moveTime)
            .SetEase(easy).OnComplete(() =>
            {
                _angleTween = _characterBody.DORotate(Vector3.zero, moveTime)
                    .SetEase(easy);
            });
        
    }

    private Vector3 ClampPositionInField(Vector3 originPosition)
    {
        float edge = GameManager.Instance.SceneController.FieldRange;
        float correctPosX = Mathf.Clamp(originPosition.x, -1 * edge, edge);
        return new Vector3(correctPosX, originPosition.y, 0);
    }

    private void KillMoveTween()
    {
        DOTween.Kill(_moveLeft);
        DOTween.Kill(_moveRight);
        DOTween.Kill(_angleTween);
    }
    
    # endregion
    
    public void TakeHit(int damage, Vector3 position)
    {
        //SlideCharacter(position);
    }

    public void SetActiveCollider(bool value)
    {
        _characterCollider.enabled = value;
    }
    

    public void ShowRainbowEffect(float time)
    {
        _showRainbowEffect.SetActive(true);
        RotateKingBoost(true);

        _rainbowEffect.SetActive(true);
        foreach (var ps in _rainbowEffect.GetComponentsInChildren<ParticleSystem>())
        {
            ParticleSystem.MainModule settings = ps.main;
           Color target = ps.main.startColor.color;
           target.a = 1;
           settings.startColor = new ParticleSystem.MinMaxGradient( target );
        }
           
        DOVirtual.DelayedCall(time - 0.5f, () =>
        {
            RotateKingBoost(false);
        });
        DOVirtual.DelayedCall(time-0.3f, () =>
        {
            foreach (var ps in _rainbowEffect.GetComponentsInChildren<ParticleSystem>())
            {
                ParticleSystem.MainModule settings = ps.main;
                Color target = ps.main.startColor.color;
                target.a = 0;
                settings.startColor = new ParticleSystem.MinMaxGradient( target );
            }
        });
        DOVirtual.DelayedCall(time, () =>
        {
            _rainbowEffect.SetActive(false);
            _showRainbowEffect.SetActive(false);
        });
    }

    private void RotateKingBoost(bool isStart)
    {
        if(_angleBoostTween!=null) _angleBoostTween.Kill();
        if (isStart)
        {
            Vector3 targetAngle = _characterBody.eulerAngles;
            targetAngle.z = -35f;
            _angleBoostTween = _characterBody.DORotate(targetAngle, 0.2f)
                .SetEase(Ease.InQuad);
        }
        else
        {
            Vector3 targetAngle = Vector3.zero;
            _angleBoostTween = _characterBody.DORotate(targetAngle, 0.5f)
                .SetEase(Ease.InOutQuad);
        }
    }

    private void Update()
    {
        //update angle
        float angleK = _kingRig.velocity.x > 1 ? 1 : -1;
        float targetZ = 15 * angleK;
        Vector3 newAngle = _characterBody.transform.eulerAngles;

        //float angleZ = Mathf.Lerp(newAngle.z, targetZ, 10 * Time.deltaTime);
        //newAngle.z = angleZ;
        //_characterBody.transform.eulerAngles = newAngle
    }

    private string _currentTrigger = "ReturnIdle";

    public void SetKingFall(bool isFall)
    {
        if (isFall)
        {
            if (_currentTrigger == "ReturnIdle")
            {
                _kingAnimator.SetTrigger("Down");
                _currentTrigger = "Down";
            }
        }
        else
        {
            if (_currentTrigger == "Down")
            {
                _kingAnimator.SetTrigger("ReturnIdle");
                _currentTrigger = "ReturnIdle";
            }
        }
    }
}
