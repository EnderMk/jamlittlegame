using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using DG.Tweening;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    public AudioSource source;
    
    public List<AudioClip> audioClipsList;

    public AudioClip endGame;
    public AudioClip mainTheme;
    public AudioClip menuTheme;
    public AudioClip startGameButton;
    public AudioClip voiceKing;


    // Start is called before the first frame update
    void Start()
    {
        if (Instance != null)
        {
            Destroy(gameObject); 
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void PlayAmbientSound(AudioClip clip)
    {
        if (source.isPlaying)
        {
            source.DOFade(0, 0.5f).OnComplete( () => PlayClip(clip));
        }
        else
        {
            PlayClip(clip);
        }

        void PlayClip(AudioClip clip)
        {
            source.clip = clip;
            source.Play();
            source.DOFade(1, 0.5f);
        }
    }

    public void PlaySFXSound(AudioClip clip)
    {
        AudioSource selectedSource = gameObject.AddComponent<AudioSource>();

        selectedSource.clip = clip;
        selectedSource.Play();
        Timer.Register(clip.length, () =>
        {
            DestroyImmediate(selectedSource);
        });
    }
}
