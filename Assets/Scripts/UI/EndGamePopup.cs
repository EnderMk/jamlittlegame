using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EndGamePopup : MonoBehaviour
{
    [SerializeField] private Image leftLine;
    [SerializeField] private Image rightLine;
    [SerializeField] private Image circle;
    [SerializeField] private List<TextMeshProUGUI> baseText;
    [SerializeField] private List<TextMeshProUGUI> interactiveText;

    private void Start()
    {
        //StartAnimation();
    }

    public void StartAnimation()
    {
        StartCoroutine(AnimationCor());
    }

    public void RestartButton()
    {
        GameManager.Instance.RestartGame();
    }

    private IEnumerator AnimationCor()
    {
        yield return FillAmountCor(leftLine, 0.06f);
        yield return FillAmountCor(rightLine, 0.05f);
        yield return FillAmountCor(circle, 0.05f);
        Color color = new Color(Color.white.r, Color.white.g, Color.white.b, 1);
        foreach (var text in baseText)
        {
            text.DOColor(color, 1).SetEase(Ease.InFlash);
        }
        foreach (var text in interactiveText)
        {
            text.DOColor(color, 1).SetEase(Ease.InFlash).OnComplete(() =>
            {
                Color color = new Color(Color.white.r, Color.white.g, Color.white.b, 0.2f);
                text.DOColor(color, 0.5f).SetEase(Ease.InFlash).OnComplete(() =>
                {
                    Color color = new Color(Color.white.r, Color.white.g, Color.white.b, 1);
                    text.DOColor(color, 0.4f).SetEase(Ease.InFlash);
                });
            });
        }
    }

    private IEnumerator FillAmountCor(Image image, float step)
    {
        while (image.fillAmount < 1)
        {
            image.fillAmount += step;
            yield return null;
        }
    }
}
