using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using UnityEngine;

public class BackgroundController : MonoBehaviour
{
    [SerializeField] private Transform allBG;
    [SerializeField] private Transform clouds;
    [SerializeField] private Transform backAirPos;
    [SerializeField] private Transform backAirSpacePos;
    [SerializeField] private Transform backSpacePos;
    [SerializeField] private Transform cloudsStartPos;
    [SerializeField] private Transform cloudsEndPos;

    [SerializeField] private SpriteRenderer planet1;
    [SerializeField] private SpriteRenderer planet2;
    [SerializeField] private Transform cloudsBack;
    [SerializeField] private List<SpriteRenderer> frontBackSprites;
    [SerializeField] private List<Sprite> spaceContentSprites;
    [SerializeField] private List<Sprite> cloudsSprites;
    [SerializeField] private List<Transform> frontBackRightPositions;
    [SerializeField] private List<Transform> frontBackLeftPositions;
    [SerializeField] private Transform stars;
    
    private bool isPalnet2;
    private bool _canUpdate = false;
    
    private void Start()
    {
        
    }

    public void StartBackground()
    {
        _canUpdate = true;
        SpawnClowds();
    }
    public void StopBackground()
    {
        _canUpdate = false;
    }
    
    private void Update()
    {

        if (!_canUpdate)
            return;
        

        stars.Rotate(new Vector3(0, 0, -0.1f));

        if (!isPalnet2 && allBG.transform.localPosition.y < -6)
        {
            planet2.gameObject.SetActive(true);
            isPalnet2 = true;
            planet1.DOFade(0, 1);
            cloudsBack.DOMove(new Vector3(cloudsBack.position.x, cloudsBack.position.y -40, cloudsBack.position.z), 2);
        }

        if (allBG.transform.localPosition.y > -130)
        {
            Vector3 target = new Vector3(allBG.position.x, allBG.position.y - 0.03f, allBG.position.z);
            allBG.position = Vector3.MoveTowards(allBG.position, target, 1);
        }
    }
    /*private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ToAir();
        }
        else if (Input.GetMouseButtonDown(1))
        {
            ToAirSpace();
        }
        else if (Input.GetMouseButtonDown(2))
        {
            ToSpace();
        }
    }*/

    public void ToAir()
    {
        MoveBack(backAirPos);
        SpawnClowds();
    }
    
    public void ToAirSpace()
    {
        MoveBack(backAirSpacePos);
        SpawnClowds();
    }
    
    public void ToSpace()
    {
        MoveBack(backSpacePos);
        StopClouds();
    }

    private void MoveBack(Transform back)
    {
        Vector3 target = new Vector3(back.position.x, back.position.y, allBG.position.z);
        allBG.transform.DOMove(allBG.transform.position, 0.3f).OnComplete(() =>
        {
            allBG.transform.DOMove(target, 0.3f);
        });
        
        MoveClouds(target);
    }

    private Tween _backCloudsTween;
    
    private void MoveClouds(Vector3 startPos)
    {
        if (_backCloudsTween != null)
            _backCloudsTween.Kill();
        clouds.transform.localPosition = cloudsStartPos.localPosition;
        _backCloudsTween = clouds.transform.DOLocalMove(cloudsEndPos.localPosition, 0.5f).SetEase(Ease.Linear);
    }

    private List<Tween> _backFrontCloudsUpTween = new List<Tween>();
    private List<Tween> _backFrontCloudsDownTween = new List<Tween>();

    private void StopClouds()
    {
        foreach (var back in _backFrontCloudsUpTween)
        {
            back.Kill();
        }
        foreach (var back in _backFrontCloudsDownTween)
        {
            back.Kill();
        }
        _backFrontCloudsUpTween.Clear();
        _backFrontCloudsDownTween.Clear();
    }

    private void SpawnClowds()
    {
        StopClouds();
        

        if (allBG.transform.localPosition.y < -60)
        {
            Tween t = transform.DOScale(0, 0);
            _backFrontCloudsUpTween.Add(t);
            _backFrontCloudsDownTween.Add(t);
            SpawnFrontBackCloud(frontBackSprites[0], 0);
        }
        else
        {
            for (int i = 0; i < frontBackSprites.Count; i++)
            {
                Tween t = transform.DOScale(0, 0);
                _backFrontCloudsUpTween.Add(t);
                _backFrontCloudsDownTween.Add(t);
            }

            for (int i = 0; i < frontBackSprites.Count; i++)
            {
                SpawnFrontBackCloud(frontBackSprites[i], i);
            }
        }
    }

    private void SpawnFrontBackCloud(SpriteRenderer frontBackSprite, int index)
    {
        if (_backFrontCloudsDownTween[index] != null)
        {
            _backFrontCloudsDownTween[index].Kill();
        }
        
        if (_backFrontCloudsDownTween[index] != null)
        {
            _backFrontCloudsUpTween[index].Kill();
        }
        
        int randomPos;
        bool isLeft;
        
        List<Transform> posList;

        if (UnityEngine.Random.Range(0, 2) == 0)
        {
            posList = frontBackLeftPositions;
            randomPos = UnityEngine.Random.Range(0, frontBackLeftPositions.Count);
            isLeft = true;
        }
        else
        {
            isLeft = false;
            posList = frontBackRightPositions;
            randomPos = UnityEngine.Random.Range(0, frontBackRightPositions.Count);
        }

        if (allBG.transform.localPosition.y < -60)
        {
            int randomSprite = UnityEngine.Random.Range(0, spaceContentSprites.Count);
            frontBackSprite.sprite = spaceContentSprites[randomSprite];

            int randomAngle = UnityEngine.Random.Range(-45, 45);
            frontBackSprite.transform.eulerAngles = new Vector3(frontBackSprite.transform.eulerAngles.x,
                frontBackSprite.transform.eulerAngles.y, randomAngle);
        }
        else
        {
            int randomSprite = UnityEngine.Random.Range(0, cloudsSprites.Count);
            frontBackSprite.sprite = cloudsSprites[randomSprite];
        }

        frontBackSprite.transform.localPosition = posList[randomPos].localPosition;

        if (isLeft)
        {
            _backFrontCloudsDownTween[index] = frontBackSprite.transform.DOLocalMoveX(frontBackSprite.transform.localPosition.x + 25, 40)
                .OnComplete(() =>
                {
                    SpawnFrontBackCloud(frontBackSprite, index);
                }).SetEase(Ease.Linear);;
            _backFrontCloudsDownTween[index] = frontBackSprite.transform.DOLocalMoveY(frontBackSprite.transform.localPosition.y - 15, 40)
                .OnComplete(() => { SpawnFrontBackCloud(frontBackSprite, index); }).SetEase(Ease.Linear);;
        }
        else
        {
            _backFrontCloudsDownTween[index] = frontBackSprite.transform.DOLocalMoveX(frontBackSprite.transform.localPosition.x - 25, 40)
                .OnComplete(() =>
                {
                    SpawnFrontBackCloud(frontBackSprite, index);
                }).SetEase(Ease.Linear);;
            _backFrontCloudsDownTween[index] = frontBackSprite.transform.DOLocalMoveY(frontBackSprite.transform.localPosition.y - 15, 40)
                .OnComplete(() =>
                {
                    SpawnFrontBackCloud(frontBackSprite, index);
                }).SetEase(Ease.Linear);;
        }
    }
}
