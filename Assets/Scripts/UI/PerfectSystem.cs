using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class PerfectSystem : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private List<string> perfects;

    public void SetPerfect()
    {
        int randomText = UnityEngine.Random.Range(0, perfects.Count);

        text.text = perfects[randomText];
        text.transform.eulerAngles = new Vector3(0, 0, 0);
        text.DOFade(1, 0);
        text.gameObject.SetActive(true);

        int r1 = UnityEngine.Random.Range(0, 255);
        int g1 = UnityEngine.Random.Range(0, 255);
        
        int r2 = UnityEngine.Random.Range(0, 255);
        int g2 = UnityEngine.Random.Range(0, 255);
        text.color = new Color32((byte) r1, (byte) g1, 255, (byte) text.color.a);
        text.DOColor(new Color32((byte)r2, (byte)g2, 255, (byte)text.color.a), 0.8f);
        
        text.transform.DOScale(2, 0.3f).OnComplete(() =>
        {
            text.transform.DOScale(1, 0.5f);
        }).SetEase(Ease.InBack);
        
        int angle = UnityEngine.Random.Range(-45, 45);
        
        text.transform.DORotate(new Vector3(0, 0, angle), 0.3f).OnComplete(() =>
        {
            text.transform.DORotate(new Vector3(0, 0, -angle), 0.5f).OnComplete(() =>
            {
                
            });
        });

        text.DOFade(0, 0.8f).OnComplete(() =>
        {
            text.gameObject.SetActive(false);
        });
    }
}
