using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Slider weightSlider;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private Image cucumber;
    [SerializeField] private Image heriing;
    [SerializeField] private Image milk;
    [SerializeField] private PerfectSystem perfectSystem;
    [SerializeField] private Image weightArrow;
    [SerializeField] private Image weightIcon;
    [SerializeField] private TextMeshProUGUI tutorial;
    [SerializeField] private EndGamePopup pop;


    private int index = 1;

    private void Start()
    {
        WeightIconShake();
    }

    public void ShowEnd()
    {
        pop.gameObject.SetActive(true);
        pop.StartAnimation();
    }
    
    public void SetActiveTutorial(bool activate)
    {
        Color targetColor = tutorial.color;
        targetColor.a = activate ? 1 : 0;
        //tutorial.DOColor(targetColor)
    }

    public void AddWeight(float weight)
    {
        weightSlider.value += weight;
        weightArrow.transform.DOScale(2, 0.2f).OnComplete(() =>
        {
            weightArrow.transform.DOScale(1, 0.4f);
        });
    }

    private void WeightIconShake()
    {
        index *= -1;
        float timing = 0.5f - weightSlider.value * 0.006f;
        if (timing < 0.05f)
            timing = 0.05f;

        weightIcon.transform.DOScale(new Vector3(1+ weightSlider.value * 0.003f, 1+ weightSlider.value * 0.003f, 1+ weightSlider.value * 0.003f), 0.3f);
        weightIcon.transform.DORotate(new Vector3(0, 0, index * (25 + weightSlider.value * 0.1f)  * 0.1f), timing).OnComplete(() =>
        {
            WeightIconShake();
        });
    }

    public void SetScore(string str)
    {
        scoreText.text = str;
    }

    public void SetCucumber()
    {
        SetScale(cucumber);
    }
    
    public void SetMilk()
    {
        SetScale(milk);
    }
    
    public void SetHerring()
    {
        SetScale(heriing);
    }

    public void SetPerfect()
    {
        perfectSystem.SetPerfect();
    }

    private void SetScale(Image image)
    {
        image.transform.DOScale(2, 0.3f).OnComplete(() =>
        {
            image.transform.DOScale(1, 0.3f).SetEase(Ease.InOutBack);;
        }).SetEase(Ease.InOutBack);;
    }
}

