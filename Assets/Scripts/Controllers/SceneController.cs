using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    [SerializeField] private KingCharacterController _kingCharacter;
    [SerializeField] public Transform bulletParent;
    [SerializeField] private float _fieldRange = 3.0f;
    [SerializeField] private float _boostTime = 5.0f;
    [SerializeField] private GameObject _boosterEffect;

    public KingCharacterController KingCharacter => _kingCharacter;
    public float FieldRange => _fieldRange;
    public float BoostTime => _boostTime;

    private int _defoultFiledOfView = -10;
    private int _boosterFieldOfView = -13;

    
    private void Start()
    {
        _boosterEffect.SetActive(false);
        EventManager.Instance.OnBoosterApply += ShowBoosterEffect;
    }


    public void ShowBoosterEffect(float time)
    {
        _kingCharacter.ShowRainbowEffect(time);
    }
    
    private void ShowBoosterEffect()
    {
        _boosterEffect.SetActive(true);
        _kingCharacter.SetActiveCollider(false);
        _kingCharacter.ShowRainbowEffect(_boostTime);
        Camera.main.transform.DOMoveZ(_boosterFieldOfView, 1.0f);
        
        DOVirtual.DelayedCall(_boostTime-1.0f, () =>
        {
            Camera.main.transform.DOMoveZ(_defoultFiledOfView, 1.0f);
        });
        
        DOVirtual.DelayedCall(_boostTime-0.3f, () =>
        {
            _boosterEffect.SetActive(false);
        });
        
        DOVirtual.DelayedCall(_boostTime, () =>
        {
            _kingCharacter.SetActiveCollider(true);
        });
    }
}
