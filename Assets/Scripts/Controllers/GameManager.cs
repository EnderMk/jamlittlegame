using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private ShootController _shootController;
    [SerializeField] private SceneController _sceneController;
    [SerializeField] private UIManager _uiManager;
    [SerializeField] private WeightManager _weightManager;
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private InputManager _inputManager;
    [SerializeField] private CameraController _cameraController;
    [SerializeField] private ItemSpawner _spawner;
    [SerializeField] private BackgroundController _backgroundController;
    [SerializeField] private Animator _startAnimation;
    
    [SerializeField] private GameObject _startGameButton;
    [SerializeField] private Image _startGameSprite1;
    [SerializeField] private Image _startGameSprite2;
    [SerializeField] private SpriteRenderer _bgAnim;

    [Header("Stuff")]
    [SerializeField] private Transform uiGamplay;

    public PlayerController PlayerController => _playerController;

    public ShootController ShootController => _shootController;
    public SceneController SceneController => _sceneController;
    public UIManager UIManager => _uiManager;
    public WeightManager WeightManager => _weightManager;
    public InputManager InputManager => _inputManager;

    public static GameManager Instance;

    private void Awake()
    {
#if UNITY_STANDALONE
        Screen.SetResolution(680, 3000, false);
        Screen.fullScreen = false;
#endif
        Application.targetFrameRate = 60;
        Instance = this;
        _inputManager.SetForceBlock(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    StartGame();
        //}
    }

    public void StartGame()
    {
        float startDelay = 7.0f;
        float showDelay = 3.0f;
        float _allStart = 1.5f;
        
        HideStartButton(_startGameSprite2, 0.0f, true);
        HideStartButton(_startGameSprite2, 1.0f, false);
        HideStartButton(_startGameSprite1, 1.0f, false);
        DOVirtual.DelayedCall(startDelay, () =>
        {
            _startGameButton.SetActive(false);
        });

        DOVirtual.DelayedCall(_allStart, () =>
        {
            _startAnimation.SetTrigger("Start");
            DOVirtual.DelayedCall(4.8f, () =>
            {
                Color targetColor = _bgAnim.color;
                targetColor.a = 0;
                _bgAnim.DOColor(targetColor, 0.5f);
            });
            DOVirtual.DelayedCall(startDelay, () =>
            {
                _backgroundController.StartBackground();
                ShowGamplayUI(showDelay);
                PlayerController.MoveKingToStart(showDelay, () =>
                {
                    _cameraController.StartFollowTarget();
                    _spawner.StartSpawn();
                    _inputManager.SetForceBlock(false);
                });
            });
        });
    }

    private void HideStartButton(Image spritetoHide, float delay, bool show)
    {
        DOVirtual.DelayedCall(delay, () =>
        {
            Color targetColor = spritetoHide.color;
            targetColor.a = show ? 1 : 0;
            spritetoHide.DOColor(targetColor, 0.5f);
        });

    }



    public void EndGame()
    {
        _spawner.StopSpawn();
        _inputManager.SetForceBlock(true);
        DOVirtual.DelayedCall(2f, () =>
        {
            _uiManager.ShowEnd();
        });
    }

    
    private void ShowGamplayUI(float time)
    {
        //move ui gameplay
        uiGamplay.DOLocalMoveY(0, time);
    }
    
    
}
