using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Unity.Mathematics;
using UnityEngine;

public class ShootController : MonoBehaviour
{
    [SerializeField] private GameObject _bulletPrefab;
    [SerializeField] private float _fireRate = 0.5f;
    [SerializeField] private GameObject[] bulletBlowEffects = new GameObject[]{};
    [SerializeField] private Transform[] bulletSpawnPoints = new Transform[]{};
    [SerializeField] private float _centerRange = 1.5f;
    [SerializeField] private float _range = 2.5f;

    //for bullet shoot
    private float _timeToFire;
    private Queue<GameObject> _bulletPool = new Queue<GameObject>();
    private int _spawnPointIndex = 0;
    
    public void ShootBullet(Vector3 mousePos)
    {
        UpdateShootPos(mousePos);
        Shoot();
    }

    private void UpdateShootPos(Vector3 mousePos)
    {
        Vector3 mousePosTemp = mousePos;
        mousePosTemp.z = 10;
        Vector3 worldPos = Camera.main.ScreenToWorldPoint(mousePosTemp);
        //clamp mouse x in range
        float shootPos = Mathf.Clamp(worldPos.x, -1*_range, _range);
        if (shootPos < (_centerRange * -1))
        {
            _spawnPointIndex = 0;
        }
        else if (shootPos > _centerRange)
        {
            _spawnPointIndex = 2;
        }else
        {
            _spawnPointIndex = 1;
        }
    }
    
    private  void Shoot()
    {
        if (Time.time > _timeToFire)
        {
            ShootBullets();
            _timeToFire = Time.time + 1 / _fireRate;
        }
    }
    
    private void ShootBullets()
    {
        Bullet bullet = _bulletPool.Count > 0
            ? GetBullet()
            : CreateBullet();
        bullet.StartTimerToSelfDestroy();
    }
    
    private Bullet CreateBullet()
    {
        GameObject bulletObject = Instantiate(_bulletPrefab, GetSpawnPosition(), Quaternion.identity);
        Bullet bullet = bulletObject.GetComponent<Bullet>();
        bulletObject.transform.SetParent(GameManager.Instance.SceneController.bulletParent);
        bullet.Init();
        bullet.UpdateBulletParticle();
        bullet.OnBulletDestroy = OnBulletDestroy;
        return bullet;
    }

    private Bullet GetBullet()
    {
        GameObject bulletObject = _bulletPool.Dequeue();
        Bullet bullet = bulletObject.GetComponent<Bullet>();
        bulletObject.transform.position = GetSpawnPosition();
        bulletObject.SetActive(true);
        bullet.UpdateBulletParticle();
        return bullet;
    }

    private Vector3 GetSpawnPosition()
    {
        return bulletSpawnPoints[_spawnPointIndex].position;
    }
    
    private void OnBulletDestroy(GameObject bulletObject, Bullet bulletComp)
    {
        ShowBlowEffect(bulletObject.transform.position, bulletComp.CurrentParticleIndex);
        bulletObject.transform.position = Vector3.zero;
        bulletObject.SetActive(false);
        _bulletPool.Enqueue(bulletObject);
    }

    private void ShowBlowEffect(Vector3 pos, int effectIndex)
    {
        Transform parent = GameManager.Instance.SceneController.KingCharacter.transform;
        GameObject effect = Instantiate(bulletBlowEffects[effectIndex], pos, quaternion.identity, parent);
        DOVirtual.DelayedCall(0.6f, () =>
        {
            effect.transform.parent = null;
            Destroy(effect);
        });
    }
}
