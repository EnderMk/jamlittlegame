using System;
using System.Collections;
using UnityEngine;

namespace Controllers
{
    public class ScoreController : MonoBehaviour
    {
        [SerializeField] private int maxScorePoints = 6;
        
        private KingCharacterController kingCharacter;
        private float nowPosition;
        private int nowScore;
        
        private void Start()
        {
            kingCharacter = GameManager.Instance.SceneController.KingCharacter;
            StartCoroutine(AddScoreCor());
        }

        private IEnumerator AddScoreCor()
        {
            while (true)
            {
                nowScore += 1;
                yield return new WaitForSeconds(0.1f);
            }
        }

        private void FixedUpdate()
        {
            /*if (kingCharacter.transform.localPosition.y > nowPosition)
            {
                nowScore += (int)(kingCharacter.transform.localPosition.y*10 - nowPosition*10);
                nowPosition = kingCharacter.transform.localPosition.y;
            }*/

            if (nowScore.ToString().Length > maxScorePoints)
            {
                GameManager.Instance.UIManager.SetScore(nowScore.ToString());
            }
            else
            {
                for (int i = 1; i < maxScorePoints; i++)
                {
                    if (nowScore.ToString().Length == i)
                    {
                        string str = "";
                        for (int j = 0; j < maxScorePoints-i; j++)
                        {
                            str += "0";
                        }
                        str += nowScore;
                        GameManager.Instance.UIManager.SetScore(str);
                        break;
                    }
                }
            }
        }
    }
}