using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _range = 2.5f;
    [SerializeField] private KingCharacterController _king; 
    [SerializeField] private float _speed = 2.5f;
    [SerializeField] private float _flyUpspeed = 2.5f;
    [SerializeField] private float _flyDownSpeed = 2.5f;
    [SerializeField] private float _rangeOffestX = 0.0f;
    [SerializeField] private Transform _boosterPos;

    [SerializeField] private float _delayMove = 0.5f;
    private float _startTouchTime;
    private bool isTouch = false;
    private bool _forceBlock = true;
    private Tween boostTween;
    private Tween boostMoveTween;
    
    //[SerializeField] private float _massMod = 1;
    private float _startMassMod = 1;
    private float _maxAddMass = 1;

    public float MassMod
    {
        set
        {
            _startMassMod += (value/800 *_maxAddMass);
        }
        get
        {
            return _startMassMod;
        }
    }

    public void ResetMass()
    {
        _startMassMod = 1;
    }

    private void Start()
    {
        EventManager.Instance.OnBoosterApply += BoosteMove;
    }

    
    
    public void MoveKing(Vector3 mousePos)
    {
        if (_forceBlock)
            return;
        if (IsCanMoveKing())
        {
            UpdadeKingAnim(false);
            if (_king == null)
                return;
            Vector3 targetPos = GetKingPosition(mousePos);
            _king.transform.position = Vector3.Lerp(_king.transform.position, targetPos, _speed * Time.deltaTime);
        }
        else
        {
            FallKing();
        }
    }
    
    public void MoveKingToStart(float time, Action onComplete)
    {
        _forceBlock = true;
        Vector3 target = _boosterPos.position;
        _king.SetKingFall(false);
        _king.transform.DOMove(target, time).SetEase(Ease.OutQuad).OnComplete(() =>
        {
            _king.SetKingFall(true);
            _forceBlock = false;
            onComplete?.Invoke();
        });
    }
    
    private void UpdadeKingAnim(bool isFall)
    {
        _king.SetKingFall(isFall);
    }
    
    public void BoosteMove()
    {
        _forceBlock = true;
        StopMoveKing();
        Debug.Log("BoosteMove");
        Vector3 targetPos = GetKingBoostPos();
        
        if(boostMoveTween!=null) boostMoveTween.Kill();
        boostMoveTween = _king.transform.DOMove(targetPos, 1.0f);
        
        GameManager.Instance.InputManager.BlockInput();
        
        if(boostTween!=null) boostTween.Kill();
        boostTween = DOVirtual.DelayedCall(GameManager.Instance.SceneController.BoostTime, () =>
        {
            _forceBlock = false;
        });
    }
    
    public void StartMoveKing()
    {
        if (!isTouch)
        {
            isTouch = true;
            _startTouchTime = Time.time;
        }
    }
    
    public void StopMoveKing()
    {
        if (isTouch)
        {
            isTouch = false;
        }
    }

    private bool IsCanMoveKing()
    {
        if (Time.time >= _startTouchTime + _delayMove)
        {
            return true;
        }

        return false;
    }

    public void FallKing()
    {
        if (_forceBlock)
            return;
        UpdadeKingAnim(true);
        Vector3 newPos = _king.transform.position;
        newPos += Vector3.down *_flyDownSpeed * Time.deltaTime * _startMassMod;
        _king.transform.position = newPos;
    }


    
    private Vector3 GetKingPosition(Vector3 mousePos)
    {
        //convert to worls
        Vector3 mousePosTemp = mousePos;
        mousePosTemp.z = 10;
        Vector3 worldPos = Camera.main.ScreenToWorldPoint(mousePosTemp);
        //clamp in range
        float min = (-1 * _range) + _rangeOffestX;
        float max = _range + _rangeOffestX;
        float kingPos = Mathf.Clamp(worldPos.x, min, max);
        Vector3 newPos = _king.transform.position;
        newPos.x = kingPos;
        
        //apply fly up
        newPos += Vector3.up *_flyUpspeed / _startMassMod;

        return newPos;
    }

    private Vector3 GetKingBoostPos()
    {
        Vector3 newPos = Vector3.zero;
        //apply fly up
        newPos += Vector3.up *_flyUpspeed*10;

        return _boosterPos.position;
    }

    private void Update()
    {
        if (!_forceBlock && _king.transform.position.y < -20.0f)
        {
            GameManager.Instance.EndGame();
            _forceBlock = true;
        }
    }
}
