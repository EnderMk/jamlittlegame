using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private bool _forceBlock = false;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            EventManager.Instance.OnBoosterApply?.Invoke();
        }
        
        if (_forceBlock)
            return;
        
        if (Input.GetMouseButtonDown(0))
        {
            GameManager.Instance.PlayerController.StartMoveKing();
        }else if (Input.GetMouseButtonUp(0))
        {
            GameManager.Instance.PlayerController.StopMoveKing();
        }
        
        if (Input.GetMouseButton(0))
        {
            Touch();
        }
        else
        {
            NoTouch();
        }
    }

    private void Touch()
    {
        GameManager.Instance.PlayerController.MoveKing(Input.mousePosition);
        GameManager.Instance.ShootController.ShootBullet(Input.mousePosition);
    }

    private void NoTouch()
    {
        GameManager.Instance.PlayerController.StopMoveKing();
        GameManager.Instance.PlayerController.FallKing();
    }

    public void BlockInput()
    {
        _forceBlock = true;
        DOVirtual.DelayedCall(GameManager.Instance.SceneController.BoostTime, () =>
        {
            _forceBlock = false;
        });
    }

    public void SetForceBlock(bool value)
    {
        _forceBlock = value;
    }
    
    
}
