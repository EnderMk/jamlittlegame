using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeightManager : MonoBehaviour
{
    [SerializeField] private float step = 0.01f;
    
    private float maxWeight = 100;
    private float nowWeight = 0;
    private float freeWeight;

    private float actualWeight = 0;
    private bool isBooster;

    private bool _canPlay = true;
    
    private void Start()
    {
        EventManager.Instance.OnBoosterApply += ReternWeight;
    }

    public void StopWeight()
    {
        _canPlay = false;
    }
    
    public void AddWeight(float weight)
    {
        freeWeight += weight;
        actualWeight += weight;
        UpdatePlayerMass();
        CheckEndGame();
    }

    public void ReternWeight()
    {
        isBooster = true;
    }
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            actualWeight = 100;
        }
        
        if (!_canPlay)
            return;
        
        if (isBooster && nowWeight > 0)
        {
            nowWeight -= step*10;
            GameManager.Instance.UIManager.AddWeight(-step*10);
        }
        else if(isBooster && nowWeight <= 0)
        {
            nowWeight = 0;
            isBooster = false;
            actualWeight = 0;
            ResetPlayerMass();
        }
        else if (freeWeight > 0)
        {
            nowWeight += step;
            freeWeight -= step;
            GameManager.Instance.UIManager.AddWeight(step);
        }
        else if (freeWeight < 0)
        {
            freeWeight = 0;
        }
    }

    private void UpdatePlayerMass()
    {
        GameManager.Instance.PlayerController.MassMod = actualWeight;
    }
    private void ResetPlayerMass()
    {
        GameManager.Instance.PlayerController.ResetMass();
    }

    private void CheckEndGame()
    {
        if (actualWeight > 100)
        {
            GameManager.Instance.EndGame();
        }
    }
    
}
