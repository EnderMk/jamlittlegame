using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public static EventManager Instance;
    public Action OnEatingNow;
    public Action OnCharacterLatched;
    public Action OnBoosterApply;

    private void Awake()
    {
        Instance = this;
    }
}
