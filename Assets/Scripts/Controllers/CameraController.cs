using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private float _minValue;

    private float _lastY;
    private bool _followTarget = false;
    
    private void Awake()
    {
        _lastY = transform.position.y +_minValue;
    }

    public void StartFollowTarget()
    {
        _followTarget = true;
    }
    
    // Update is called once per frame
    void LateUpdate()
    {
        if (_target == null || !_followTarget)
            return;
        //update camera position
        if (_target.transform.position.y > _lastY)
        {
            Vector3 newCameraPos = transform.position;
            newCameraPos.y = _target.transform.position.y-_minValue;
            transform.position = newCameraPos;
            _lastY = _target.transform.position.y;
        }
    }
    
}
