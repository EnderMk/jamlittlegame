using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float speed = 35.0f;
    [SerializeField] private float delayDestroy = 7.0f;
    [SerializeField] private int damage = 1;
    [SerializeField] private GameObject[] bulletParticlesTrail = new GameObject[]{};

    private Rigidbody2D bulletRig;
    public Action<GameObject, Bullet> OnBulletDestroy;

    private int _currentParticleIndex = 0;
    public int CurrentParticleIndex => _currentParticleIndex;

    public void Init()
    {
    }

    public void UpdateBulletParticle()
    {
        _currentParticleIndex = Random.Range(0, 2);
        bulletParticlesTrail[_currentParticleIndex].SetActive(true);
    }
    
    private void Start()
    {
        bulletRig = GetComponent<Rigidbody2D>();
        for (int i = 0; i < 2; i++)
        {
            bulletParticlesTrail[i].SetActive(false);
        }
    }
    
    private void FixedUpdate()
    {
        Move();
    }

    public void StartTimerToSelfDestroy()
    {
        StartCoroutine(StartTimerToSelfDestroy(delayDestroy));
    }

    IEnumerator StartTimerToSelfDestroy(float delayDestroy)
    {
        yield return new WaitForSeconds(delayDestroy);
        if (gameObject.activeSelf)
            OnBulletDestroy(gameObject, this);
    }
    
    private void Move()
    {
        Vector3 targetPos = GetTargetPos();
        bulletRig.transform.position = Vector3.MoveTowards( transform.position, targetPos, speed*Time.deltaTime);
    }
    
    private Vector3 GetTargetPos()
    {
        if (GameManager.Instance.SceneController.KingCharacter != null)
            return GameManager.Instance.SceneController.KingCharacter.transform.position;
        
        return Vector3.zero;
    }

    private void StopMove()
    {
        bulletRig.velocity = Vector2.zero;
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        IHitObject hitObject = other.GetComponent<IHitObject>();
        
        if ( hitObject != null )
        {
            hitObject.TakeHit(damage, transform.position);
            DisableParticles();
            OnBulletDestroy(gameObject, this);
        }
    }
    
    
    private void DisableParticles()
    {
        foreach (var particle in bulletParticlesTrail)
        {
            StopMove();
            particle.SetActive(false);
        }
    }
    
}
