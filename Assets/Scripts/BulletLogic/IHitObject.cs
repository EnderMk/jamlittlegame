using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHitObject
{
    public void TakeHit(int damage, Vector3 hitPosition);
}
